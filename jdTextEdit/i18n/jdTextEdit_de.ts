<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
  <context>
    <name>BannerWidgets</name>
    <message>
      <location filename="..\gui\BannerWidgets\SimpleMessageBanner.py" line="9" />
      <source>OK</source>
      <translation type="unfinished">OK</translation>
    </message>
  </context>
  <context>
    <name>DateTimeWindow</name>
    <message>
      <location filename="..\gui\DateTimeWindow.py" line="39" />
      <source>Add</source>
      <translation type="unfinished">Hinzufügen</translation>
    </message>
    <message>
      <location filename="..\gui\DateTimeWindow.py" line="40" />
      <source>Edit</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\DateTimeWindow.py" line="41" />
      <source>Remove</source>
      <translation type="unfinished">Löschen</translation>
    </message>
    <message>
      <location filename="..\gui\DateTimeWindow.py" line="109" />
      <source>Invalid</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\DateTimeWindow.py" line="136" />
      <source>Enter format</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\DateTimeWindow.py" line="136" />
      <source>Please enter a format</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\DateTimeWindow.py" line="144" />
      <source>Invalid format</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\DateTimeWindow.py" line="144" />
      <source>This format is invalid</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>DebugInfoWindow</name>
    <message>
      <location filename="..\gui\DebugInfoWindow.py" line="18" />
      <source>Copy to Clipboard</source>
      <translation>In die Zwischenablage kopieren</translation>
    </message>
    <message>
      <location filename="..\gui\DebugInfoWindow.py" line="19" />
      <source>Close</source>
      <translation>Schließen</translation>
    </message>
    <message>
      <location filename="..\gui\DebugInfoWindow.py" line="37" />
      <source>Debug Information</source>
      <translation>Debuginformationen</translation>
    </message>
  </context>
  <context>
    <name>EditTabWidget</name>
    <message>
      <location filename="..\gui\EditTabWidget.py" line="114" />
      <source>Set custom tab name</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\EditTabWidget.py" line="119" />
      <source>Remove custom tab name</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\EditTabWidget.py" line="126" />
      <source>Enter name</source>
      <translation type="unfinished">Namen eingeben</translation>
    </message>
    <message>
      <location filename="..\gui\EditTabWidget.py" line="126" />
      <source>Please enter a custom name for this tab</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>EditThemeWindow</name>
    <message>
      <location filename="..\gui\Windows\ManageThemesWindow\EditThemeWindow.py" line="129" />
      <source>Language:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\ManageThemesWindow\EditThemeWindow.py" line="160" />
      <source>OK</source>
      <translation type="unfinished">OK</translation>
    </message>
    <message>
      <location filename="..\gui\Windows\ManageThemesWindow\EditThemeWindow.py" line="161" />
      <source>Cancel</source>
      <translation type="unfinished">Abbrechen</translation>
    </message>
    <message>
      <location filename="..\gui\Windows\ManageThemesWindow\EditThemeWindow.py" line="176" />
      <source>Name:</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>EditorconfigTab</name>
    <message>
      <location filename="..\gui\SettingsTabs\EditorconfigTab.py" line="18" />
      <source>The editorconfig module was not found</source>
      <translation>Das editorconfig Modul wurde nicht gefunden</translation>
    </message>
  </context>
  <context>
    <name>Environment</name>
    <message>
      <location filename="..\Environment.py" line="185" />
      <source>Settings</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\Environment.py" line="186" />
      <source>Templates</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>ExportDataWindow</name>
    <message>
      <location filename="..\gui\Windows\ExportDataWindow.py" line="19" />
      <source>Choose which data you want to export</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\ExportDataWindow.py" line="28" />
      <source>OK</source>
      <translation type="unfinished">OK</translation>
    </message>
    <message>
      <location filename="..\gui\Windows\ExportDataWindow.py" line="29" />
      <source>Cancel</source>
      <translation type="unfinished">Abbrechen</translation>
    </message>
    <message>
      <location filename="..\gui\Windows\ExportDataWindow.py" line="46" />
      <source>Export data</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\ExportDataWindow.py" line="79" />
      <source>Zip Files</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\ExportDataWindow.py" line="80" />
      <source>All Files</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\ExportDataWindow.py" line="94" />
      <source>Export complete</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\ExportDataWindow.py" line="94" />
      <source>You data were successfully exported</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>Functions</name>
    <message>
      <location filename="..\Functions.py" line="88" />
      <location filename="..\Functions.py" line="83" />
      <source>Terminal emulator not found</source>
      <translation>Terminalemulator nicht gefunden</translation>
    </message>
    <message>
      <location filename="..\Functions.py" line="84" />
      <source>Your custom terminal emulator was not found</source>
      <translation>Der benutzerdefinierte Terminalemulator wurde nicht gefunden</translation>
    </message>
    <message>
      <location filename="..\Functions.py" line="89" />
      <source>The terminal emulator of the system was not found. Try setting a custom one in the Settings.</source>
      <translation>Der Terminalemulator des Systems wurde nicht gefunden. Versuche einen Benutzerdefinierten in den Einstellungen zu setzen.</translation>
    </message>
    <message>
      <location filename="..\Functions.py" line="326" />
      <source>Line {{line}} Column {{column}}: {{message}}</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>HHHH</name>
    <message>
      <location filename="..\gui\Windows\ManageThemesWindow\ManageThemeListWindow.py" line="16" />
      <source>Hallo</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>InterfaceTab</name>
    <message>
      <location filename="..\gui\SettingsTabs\InterfaceTab.py" line="17" />
      <source>Use custom Datetime format</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\SettingsTabs\InterfaceTab.py" line="57" />
      <location filename="..\gui\SettingsTabs\InterfaceTab.py" line="19" />
      <source>Preview</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\SettingsTabs\InterfaceTab.py" line="37" />
      <source>Format:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\SettingsTabs\InterfaceTab.py" line="59" />
      <source>Format invalid</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\SettingsTabs\InterfaceTab.py" line="59" />
      <source>This format is not valid</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>ListSelectWidget</name>
    <message>
      <location filename="..\gui\Widgets\ListSelectWidget.py" line="25" />
      <source>Add</source>
      <translation>Hinzufügen</translation>
    </message>
    <message>
      <location filename="..\gui\Widgets\ListSelectWidget.py" line="26" />
      <source>Remove</source>
      <translation>Entfernen</translation>
    </message>
    <message>
      <location filename="..\gui\Widgets\ListSelectWidget.py" line="27" />
      <source>Up</source>
      <translation>Nach oben</translation>
    </message>
    <message>
      <location filename="..\gui\Widgets\ListSelectWidget.py" line="28" />
      <source>Down</source>
      <translation>Nach unten</translation>
    </message>
  </context>
  <context>
    <name>MainWindow</name>
    <message>
      <location filename="..\gui\MainWindow.py" line="363" />
      <source>Random</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="620" />
      <source>RegExGrep</source>
      <translation>RegExGrep</translation>
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="626" />
      <source>Settings</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="643" />
      <source>Export data</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1660" />
      <location filename="..\gui\MainWindow.py" line="649" />
      <source>Import data</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="783" />
      <source>Debug information</source>
      <translation>Debuginformationen</translation>
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1684" />
      <location filename="..\gui\MainWindow.py" line="789" />
      <source>Delete all data</source>
      <translation>Alle Daten löschen</translation>
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="868" />
      <source>Manage</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1213" />
      <source>This file has some decoding errors</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1618" />
      <source>Zip Files</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1619" />
      <source>All Files</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1634" />
      <source>Invalid file</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1634" />
      <source>This file does not contains valid exported data from jdTextEdit</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1641" />
      <source>Unknown error</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1641" />
      <source>An unknown error happened</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1644" />
      <source>Are you sure you want to import the following data?</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1646" />
      <source>Date and Time: {{DateTime}}</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1647" />
      <source>Platform: {{Platform}}</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1648" />
      <source>Version: {{Version}}</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1650" />
      <source>It includes the following:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1658" />
      <source>If you continue, your data will be overwritten and jdTextEdit will restart</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1676" />
      <source>Import complete</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1676" />
      <source>The Import is now completed. jdTextEdit will now close.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\MainWindow.py" line="1684" />
      <source>This will delete all data of jdTextEdit. After that, jdTextEdit will behave like the first run. jdTexEdit will exit after doing that. Are you sure?</source>
      <translation>Alle Daten von jdTextEdit werden gelöscht. Danch wird sich jdTextEdit wie beim ersten Start verhalten. jdTextEdit beendet sich nach dem löschen. Willst du Fortfahren?</translation>
    </message>
  </context>
  <context>
    <name>ManageTemplatesWindow</name>
    <message>
      <location filename="..\gui\ManageTemplatesWindow.py" line="14" />
      <source>Delete</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\ManageTemplatesWindow.py" line="15" />
      <source>Rename</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\ManageTemplatesWindow.py" line="16" />
      <source>Export</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\ManageTemplatesWindow.py" line="17" />
      <source>Close</source>
      <translation type="unfinished">Schließen</translation>
    </message>
    <message>
      <location filename="..\gui\ManageTemplatesWindow.py" line="36" />
      <source>Manage templates</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\ManageTemplatesWindow.py" line="55" />
      <source>Delete {{name}}</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\ManageTemplatesWindow.py" line="55" />
      <source>Are you sure you want to delete {{name}}?</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\ManageTemplatesWindow.py" line="103" />
      <location filename="..\gui\ManageTemplatesWindow.py" line="85" />
      <location filename="..\gui\ManageTemplatesWindow.py" line="62" />
      <source>Error</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\ManageTemplatesWindow.py" line="62" />
      <source>A error occurred while deleting</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\ManageTemplatesWindow.py" line="72" />
      <source>Enter name</source>
      <translation type="unfinished">Namen eingeben</translation>
    </message>
    <message>
      <location filename="..\gui\ManageTemplatesWindow.py" line="72" />
      <source>Please enter the new name</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\ManageTemplatesWindow.py" line="78" />
      <source>Name exists</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\ManageTemplatesWindow.py" line="78" />
      <source>There is already a template with this name</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\ManageTemplatesWindow.py" line="85" />
      <source>A error occurred while renaming</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\ManageTemplatesWindow.py" line="103" />
      <source>A error occurred while exporting</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>ManageThemeListWindow</name>
    <message>
      <location filename="..\gui\Windows\ManageThemesWindow\ManageThemeListWindow.ui" line="0" />
      <source>Dialog</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\ManageThemesWindow\ManageThemeListWindow.ui" line="0" />
      <source>New</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\ManageThemesWindow\ManageThemeListWindow.ui" line="0" />
      <source>Edit</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\ManageThemesWindow\ManageThemeListWindow.ui" line="0" />
      <source>Copy</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\ManageThemesWindow\ManageThemeListWindow.ui" line="0" />
      <source>Delete</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\ManageThemesWindow\ManageThemeListWindow.ui" line="0" />
      <source>Import</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\ManageThemesWindow\ManageThemeListWindow.ui" line="0" />
      <source>Export</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\ManageThemesWindow\ManageThemeListWindow.ui" line="0" />
      <source>Close</source>
      <translation type="unfinished">Schließen</translation>
    </message>
  </context>
  <context>
    <name>ProjectWidget</name>
    <message>
      <location filename="..\gui\SidebarWidgets\ProjectWidget.py" line="132" />
      <location filename="..\gui\SidebarWidgets\ProjectWidget.py" line="41" />
      <source>Projects</source>
      <translation>Projekte</translation>
    </message>
    <message>
      <location filename="..\gui\SidebarWidgets\ProjectWidget.py" line="118" />
      <source>New Project</source>
      <translation>Neues Projekt</translation>
    </message>
    <message>
      <location filename="..\gui\SidebarWidgets\ProjectWidget.py" line="124" />
      <source>Remove Project</source>
      <translation>Projekt löschen</translation>
    </message>
  </context>
  <context>
    <name>RegExGrepListDialog</name>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepListDialog.py" line="56" />
      <source>Enter name</source>
      <translation>Namen eingeben</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepListDialog.py" line="56" />
      <source>Please enter a Name under which you want to save your RegEx</source>
      <translation>Bitte gib den Namen ein, unter dem du den RegEx speichern möchtest</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepListDialog.py" line="62" />
      <source>Already taken</source>
      <translation>Bereits vergeben</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepListDialog.py" line="62" />
      <source>This name has already been taken</source>
      <translation>Der Name ist bereits vorhanden</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepListDialog.py" line="73" />
      <source>Remove RegEx</source>
      <translation>RegEx löschen</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepListDialog.py" line="73" />
      <source>Are you sure you want to remove the selected RegEx?</source>
      <translation>Bist du sicher, dass du den ausgewählten RegEx löschen möchtest?</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepListDialog.ui" line="0" />
      <source>Saved RegEx</source>
      <translation>Gespeicherte RegEx</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepListDialog.ui" line="0" />
      <source>Search</source>
      <translation>Suche</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepListDialog.ui" line="0" />
      <source>Load</source>
      <translation>Laden</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepListDialog.ui" line="0" />
      <source>Save</source>
      <translation>Speichern</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepListDialog.ui" line="0" />
      <source>Remove</source>
      <translation>Löschen</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepListDialog.ui" line="0" />
      <source>Cancel</source>
      <translation>Abbrechen</translation>
    </message>
  </context>
  <context>
    <name>RegExGrepWindow</name>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepWindow.py" line="25" />
      <source>No results</source>
      <translation>Keine Ergebnisse</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepWindow.py" line="25" />
      <source>There are no results to copy</source>
      <translation>Es gibt keine Ergebnisse zum kopieren</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepWindow.py" line="33" />
      <source>Data copied</source>
      <translation>Daten kopiert</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepWindow.py" line="33" />
      <source>The content of the results List has been copied into your clipboard</source>
      <translation>Die Liste mit den Ergebnissen wurde erfolgreich kopiert</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepWindow.py" line="37" />
      <source>No RegEx</source>
      <translation>Kein RegEx</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepWindow.py" line="37" />
      <source>You have not entered a RegEx</source>
      <translation>Es wurde kein RegEx angegeben</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepWindow.py" line="41" />
      <source>Invalid RegEx</source>
      <translation>Ungültiger RegEx</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepWindow.py" line="49" />
      <source>Nothing found</source>
      <translation>Nichts gefunden</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepWindow.py" line="49" />
      <source>The given RegEx haven't found anything</source>
      <translation>Der angegebene RegEx erzielte keine Treffer</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepWindow.ui" line="0" />
      <source>RegExGrep</source>
      <translation>RegEx Grep</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepWindow.ui" line="0" />
      <source>RegEx Grep searches the Document with the given RegEx and puts the output into a List</source>
      <translation>RegEx Grep durchsucht das Dokument anhand des angegebenen RegEx und schreibt die Ergebnisse in eine Liste</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepWindow.ui" line="0" />
      <source>RegEx:</source>
      <translation>RegEx:</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepWindow.ui" line="0" />
      <source>Saved RegEx</source>
      <translation>Gespeicherte RegEx</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepWindow.ui" line="0" />
      <source>Copy to Clipboard</source>
      <translation>In die Zwischenablage kopieren</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepWindow.ui" line="0" />
      <source>OK</source>
      <translation>OK</translation>
    </message>
    <message>
      <location filename="..\gui\Tools\RegExGrep\RegExGrepWindow.ui" line="0" />
      <source>Cancel</source>
      <translation>Abbrechen</translation>
    </message>
  </context>
  <context>
    <name>SearchAndReplaceWindow</name>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.py" line="31" />
      <source>Invalid regular expression</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Search and Replace</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Text</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Find what:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Replace with:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Mode</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Normal</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Extended</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Regular expression</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Replace in</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Selection</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Current document</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>GroupBox</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>CheckBox</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Placeholder</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Use placeholder for result in replace text</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Placeholder:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Replace</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Replace All</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Save State</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Load State</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="..\gui\Windows\SearchAndReplaceWindow\SearchAndReplaceWindow.ui" line="0" />
      <source>Close</source>
      <translation type="unfinished">Schließen</translation>
    </message>
  </context>
  <context>
    <name>StatusBarTab</name>
    <message>
      <location filename="..\gui\SettingsTabs\StatusBarTab.py" line="17" />
      <source>Left</source>
      <translation>Links</translation>
    </message>
    <message>
      <location filename="..\gui\SettingsTabs\StatusBarTab.py" line="18" />
      <source>Right</source>
      <translation>Rechts</translation>
    </message>
    <message>
      <location filename="..\gui\SettingsTabs\StatusBarTab.py" line="46" />
      <source>Statusbar</source>
      <translation>Statusbar</translation>
    </message>
  </context>
  <context>
    <name>StatusBarWidgets</name>
    <message>
      <location filename="..\gui\StatusBarWidgets\CursorPosWidget.py" line="13" />
      <source>Cursor position</source>
      <translation>Cursorposition</translation>
    </message>
    <message>
      <location filename="..\gui\StatusBarWidgets\CursorPosWidget.py" line="18" />
      <source>Ln {line}, Col {column}</source>
      <translation>Z. {line}, Sp. {column}</translation>
    </message>
    <message>
      <location filename="..\gui\StatusBarWidgets\EncodingWidget.py" line="13" />
      <source>Encoding</source>
      <translation>Kodierung</translation>
    </message>
    <message>
      <location filename="..\gui\StatusBarWidgets\EolWidget.py" line="19" />
      <source>End of Line</source>
      <translation>Zeilenende</translation>
    </message>
    <message>
      <location filename="..\gui\StatusBarWidgets\LanguageWidget.py" line="13" />
      <source>Language</source>
      <translation>Sprache</translation>
    </message>
    <message>
      <location filename="..\gui\StatusBarWidgets\PathWidget.py" line="13" />
      <source>Path</source>
      <translation>Pfad</translation>
    </message>
  </context>
  <context>
    <name>TerminalEmulatorTab</name>
    <message>
      <location filename="..\gui\SettingsTabs\TerminalEmulatorTab.py" line="11" />
      <source>Use system terminal emulator</source>
      <translation>Terminalemulator des Systems benutzen</translation>
    </message>
    <message>
      <location filename="..\gui\SettingsTabs\TerminalEmulatorTab.py" line="12" />
      <source>Use custom terminal emulator</source>
      <translation>Eigenen Terminalemulator benutzen</translation>
    </message>
    <message>
      <location filename="..\gui\SettingsTabs\TerminalEmulatorTab.py" line="15" />
      <source>The executable name e.g. xterm</source>
      <translation>Der Name der ausführbaren Datei z.B. xterm</translation>
    </message>
    <message>
      <location filename="..\gui\SettingsTabs\TerminalEmulatorTab.py" line="20" />
      <source>You can set the terminal emulator that is used when executing commands here</source>
      <translation>Du kannst hier den Terminalemulator, der bei Befehlen benutzt wird, festlegen</translation>
    </message>
    <message>
      <location filename="..\gui\SettingsTabs\TerminalEmulatorTab.py" line="44" />
      <source>Terminal emulator</source>
      <translation>Terminalemulator</translation>
    </message>
  </context>
</TS>
